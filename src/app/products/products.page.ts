import { Component, OnInit } from '@angular/core';
import { Cloth } from '../cloth/cloth';
import { ClothService } from '../cloth/cloth.service';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-products',
  templateUrl: 'products.page.html',
  styleUrls: ['products.page.scss'],
})
export class ProductsPage implements OnInit {

  items = Array<Cloth>();

  constructor( private clothService: ClothService, private router: Router, public toastController: ToastController) {
    
  }

  ngOnInit() {
    this.items = this.clothService.items;
    console.log(this.clothService.items);
    this.openToast();
  }

  openDetails(item){
    this.clothService.items = item;
    this.router.navigate(['../productdetails']);
  }

  async openToast() {
    const toast = await this.toastController.create({
      message: 'Please understand that you cannot buy anything throughout our app, because a personal meeting for individual design is necessary.',
      buttons: [
        {
          text: 'Okay',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    });
    toast.present();
  }
  

}
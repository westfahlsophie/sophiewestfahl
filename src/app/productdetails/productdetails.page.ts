import { Component, OnInit } from '@angular/core';
import { ClothService } from '../cloth/cloth.service';
import { Cloth } from '../cloth/cloth';


@Component({
  selector: 'app-productdetails',
  templateUrl: './productdetails.page.html',
  styleUrls: ['./productdetails.page.scss'],
})
export class ProductdetailsPage implements OnInit {

  items = Array<Cloth>();

  constructor (private clothService: ClothService) { }

  ngOnInit() {
    this.items = this.clothService.items;

  }

}

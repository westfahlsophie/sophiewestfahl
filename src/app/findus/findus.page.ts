import { Component, ElementRef, NgZone, ViewChild } from '@angular/core';
import {Geolocation} from '@ionic-native/geolocation/ngx';

declare var google: any;

@Component({
  selector: 'app-findus',
  templateUrl: './findus.page.html',
  styleUrls: ['./findus.page.scss'],
})
export class FindusPage {

  @ViewChild('Map', {static: false}) mapElement: ElementRef;
    map: any;
    mapOptions: any;
    location = {lat: 48.62031936645508, lng: 10.025246620178223};
    markerOptions: any = {position: null, map: null, title: null};
    marker: any;
    apiKey: any = 'AIzaSyCxo2KR98bhwA2WhuJ__ByWM3D25kWznkk'; 
    

  constructor(public zone: NgZone, public geolocation: Geolocation) { 

    const script = document.createElement('script');
    script.id = 'googleMap';
    
    if (this.apiKey) {
        script.src = 'https://maps.googleapis.com/maps/api/js?key=' + this.apiKey;
    } else {
        script.src = 'https://maps.googleapis.com/maps/api/js?key=';
    }
    
    document.head.appendChild(script);
      
      this.mapOptions = {
          center: this.location,
          zoom: 14,
          mapTypeControl: false
      };
      setTimeout(() => {
          this.map = new google.maps.Map(this.mapElement.nativeElement, this.mapOptions);
  
          //Marker
          this.markerOptions.position = this.location;
          this.markerOptions.map = this.map;
          this.markerOptions.title = 'My Location';
          this.marker = new google.maps.Marker(this.markerOptions);
      }, 3000);

}

}

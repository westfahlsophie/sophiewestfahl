import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FindusPage } from './findus.page';

describe('FindusPage', () => {
  let component: FindusPage;
  let fixture: ComponentFixture<FindusPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FindusPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FindusPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

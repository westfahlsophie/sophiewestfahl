import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FindusPage } from './findus.page';

const routes: Routes = [
  {
    path: '',
    component: FindusPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FindusPageRoutingModule {}

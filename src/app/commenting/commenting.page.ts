import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

import{ Feedback } from '../feedback/feedback';
import{ FeedbackService } from '../feedback/feedback.service';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-commenting',
  templateUrl: './commenting.page.html',
  styleUrls: ['./commenting.page.scss'],
})
export class CommentingPage implements OnInit {

  name: string;
  email: string;
  scale: number;
  comment: string;

  feedbacks= Array<Feedback>();

  constructor(private router: Router, public feedbackService: FeedbackService, private storage: Storage, public toastController: ToastController) {
    this.feedbacks;
    console.log(this.feedbacks);
   }

  ngOnInit() {
    this.scale = 5;

    this.storage.get('feedback').then((val) => {
      this.feedbacks = JSON.parse(val);
      if(!this.feedbacks){
        this.feedbacks = [];
      }
    });

  }

  pushComment(){
    this.feedbacks.push(new Feedback(this.name, this.email, this.scale, this.comment));

    this.storage.set('feedback',JSON.stringify(this.feedbacks));
    this.router.navigate(['../comments']);

    this.openToast();

    this.name = '';
    this.email= '';
    this.scale = 5;
    this.comment = '';
  }

  pushEvent(event){
    if(event.keyCode === 13){
      this.pushComment();
    }
    
    
  }
  async openToast() {
    const toast = await this.toastController.create({
      message: 'Thank you for your feedback! It will appear after refreshing the comments.',
      duration: 3000
    });
    toast.present();
  }

}
